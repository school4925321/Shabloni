public class BasicProgramDecorator extends ProgramDecorator{
    public BasicProgramDecorator(Program program) {
        super(program);
    }

    @Override
    public boolean train() {
        super.train();
        return true;
    }

    @Override
    public void stop() {
        super.stop();
    }
}

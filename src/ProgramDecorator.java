public abstract class ProgramDecorator implements Program{
    protected Program decoratedProgram;

    public ProgramDecorator(Program program) {
        this.decoratedProgram = program;
    }

    @Override
    public boolean train() {
        decoratedProgram.train();
        return true;
    }

    @Override
    public void stop() {
        decoratedProgram.stop();
    }
}

public class StrengthDecoratedProgram extends ProgramDecorator {
    public StrengthDecoratedProgram(Program program) {
        super(program);
    }

    @Override
    public boolean train() {
        super.train();
        return true;
    }

    @Override
    public void stop() {
        super.stop();
    }
}

public class BasicProgram implements Program {

    int count = 0;

    @Override
    public boolean train() {
        System.out.println("Basic Training is starting");
        while(count != 300)
            count++;

        System.out.println("Basic Training Almost done, Keep on Going");

        return true;
    }

    @Override
    public void stop() {
        System.out.println("Basic Stop, Nicely Done");
        count = 0;
    }
}

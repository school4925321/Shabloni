public class Person implements Observer {
    private String name;
    private Program personalizedProgram;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public void startTraining(Program newProgram) {
        this.personalizedProgram = newProgram;
        if (newProgram.train()) {
            newProgram.stop();
        }
    }

    public Program getPersonalizedProgram() {
        return personalizedProgram;
    }

    public String getName() {
        return name;
    }
}

import java.util.ArrayList;
import java.util.List;

public class FitnessInstructor {
    private static FitnessInstructor instance;
    private Program currentProgram;
    private ArrayList<Person> customers;

    private FitnessInstructor() {
        this.currentProgram = new BasicProgram();
        this.customers = new ArrayList<Person>();
    }

    public static synchronized FitnessInstructor getInstance() {
        if (instance == null) {
            instance = new FitnessInstructor();
        }
        return instance;
    }

    public void setTrainingProgram(Program program) {
        this.currentProgram = program;
        startTrainingWithProgram(program);
    }

    public Program getCurrentProgram() {
        return currentProgram;
    }

    public void registerPerson(Person person) {
        customers.add(person);
    }

    private void startTrainingWithProgram(Program program) {
        for (Person person : customers) person.startTraining(program);
    }
}


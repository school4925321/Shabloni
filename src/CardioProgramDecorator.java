public class CardioProgramDecorator extends ProgramDecorator {
    public CardioProgramDecorator(Program program) {
        super(program);
    }

    @Override
    public boolean train() {
        super.train();
        return true;
    }

    @Override
    public void stop() {
        decoratedStop();
    }

    public void decoratedStop() {
        System.out.println("I stopped from Decorator :)");
        super.stop();
    }
}

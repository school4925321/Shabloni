public interface Observer {
    void startTraining(Program newProgram);
}

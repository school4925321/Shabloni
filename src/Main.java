public class Main {
    public static void main(String[] args) {
        FitnessInstructor instructor = FitnessInstructor.getInstance();

        Person person1 = new Person("Pesho");
        Person person2 = new Person("Gosho");
        Person person3 = new Person("Penka");

        Program basic = new BasicProgram();
        Program cardio = new CardioProgramDecorator(new CardioProgram());
        Program strength = new StrengthProgram();

        instructor.registerPerson(person1);
        instructor.registerPerson(person2);
        instructor.registerPerson(person3);

        instructor.setTrainingProgram(basic);
        System.out.println("");
        instructor.setTrainingProgram(cardio);
        System.out.println("");
        instructor.setTrainingProgram(strength);
        System.out.println("");
    }
}